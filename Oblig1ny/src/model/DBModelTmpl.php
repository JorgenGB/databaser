<?php
include_once("IModel.php");
include_once("Book.php");
include_once("tests/TestDBProps.php");
/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	try {
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
            $this->db = new PDO('mysql:host='.TEST_DB_HOST.';dbname'.TEST_DB_NAME.';charset=utf8',
			TEST_DB_USER, TEST_DB_PWD,
			array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXEPTION));
		}
		} catch(PDOException $dberror){
		echo 'Database error';
		echo $dberror->getMessage();
		}
    }
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		try{
		$booklist = array();
		$stmt = $this->db->query('SELECT id, title, author, description FROM book ORDER BY id');
		$booklist = $stmt->fetchAll(PDO__FETCH_ASSOC);
		} catch (PDOException $dberror) {
			echo 'Database error';
		echo $dberror->getMessage();
		}
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		try{
		$book = null;
		$result = $this->db->prepare('SELECT * WHERE id={$id} LIMIT 1');
		$result->execute();
		$book = $result->fetch();
        return $book;
		} catch(PDOException $dberror){
			echo 'Database error';
			echo $dberror-getMessage();
		}
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
		try{
		$result = $this->db->prepare('INSERT INTO book (title, author, description) ' . ' VALUES(?,?,?)');
		$result->execute(array($book->title, $book->author, $book->description));
		if($result){
			//insert id
		} else {
			echo 'Something went wrong!';
		}
		} catch (PDOException $dberror){
			echo 'Database error';
			echo $dberror-getMessage();
		}
	}

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
		try{
		$result = $this->db->prepare('UPDATE book SET title=$book->title, author=$book->author, description=$book->description ' . ' WHERE id = $book->id');
		if($result){
			echo 'Book updated successfully';
		}else echo 'Error updating book';
		} catch (PDOException $dberror){
			echo 'Database error';
			echo $dberror->getMessage();
    }
	}
    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
		$result = $this->db->prepare('DELETE FROM book WHERE id=$id');
    }
	
}

?>
<?php
include_once("IModel.php");
include_once("Book.php");
include_once("tests/TestDBProps.php");
/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	try {
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
            $this->db = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8',
                  DB_USER, DB_PWD, 
                  array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
		}
		} catch(PDOException $dberror){
		echo 'Database error';
		echo $dberror->getMessage();
		}
    }
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		try{
		$booklist = array();
		$stmt = $this->db->prepare('SELECT id, title, author, description FROM book ORDER BY id');
		$stmt->execute();
		$rows = $stmt->fetchAll();
		foreach($rows as $book){
			$booklist[] = new Book($book['title'], $book['author'],$book['description'],$book['id']);
		}
		return $booklist;
		} catch (PDOException $dberror) {
			echo 'Database error';
		echo $dberror->getMessage();
		}
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		try{
		if(!filter_var($id, FILTER_VALIDATE_INT)){
			return null;
		}
		$book = null;
		$result = $this->db->prepare('SELECT * FROM book WHERE id=:id');
		$result->bindparam(':id',$id,PDO::PARAM_INT);
		$result->execute();
		$book = $result->fetch(PDO::FETCH_ASSOC);
		if($book == false){
		return null;}
        return $book = new Book($book['title'],$book['author'],$book['description'],$book['id']);
		} catch(PDOException $dberror){
			echo 'Database error';
			echo $dberror-getMessage();
		}
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
		try{
		$result = $this->db->prepare('INSERT INTO book (title, author, description) ' . ' VALUES(?,?,?)');
		$result->execute(array($book->title, $book->author, $book->description));
		$book->id = $this->db->lastInsertId();
		} catch (PDOException $dberror){
			echo 'Database error';
		echo $dberror-getMessage();}
	}

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
		$result = $this->db->prepare('UPDATE book SET title=:title, author=:author, description=:description  WHERE id = :id');
		$result->bindParam(':title',$book->title, PDO::PARAM_STR);
		$result->bindParam(':author',$book->author, PDO::PARAM_STR);
		$result->bindParam(':description',$book->description, PDO::PARAM_STR);
		$result->bindParam(':id',$book->id, PDO::PARAM_INT);
		$result->execute();
	}
    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
		$result = $this->db->prepare('DELETE FROM book WHERE id=:id');
		$result->bindParam(':id',$id, PDO::PARAM_INT);
		$result->execute();
    }
	
}

?>